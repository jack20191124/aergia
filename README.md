[![License Badge](https://img.shields.io/badge/License-Apache%202.0-orange.svg?style=for-the-badge) ](https://www.apache.org/licenses/LICENSE-2.0)

## Aergia

![Aergia Logo](https://jack20191124.bitbucket.io/images/Aergia.png)

Enthusiastic about photography, I sometimes used shell commands to manipulate some of my photos using some
[ImageMagick](https://imagemagick.org/script/index.php). However, I found some repetitive tasks were not efficiently
done, which led to the idea of project [Aergia](https://en.wikipedia.org/wiki/Aergia), which turned multiple ImageMagick
commands into a single shell script in order to quickly complete a task.

Later on, I found it useful to expand the scope of Aergia beyond image processing, such as Docker operations and
software testings. Aergia, therefore, evolves to a collection of shell scripts that automates general tasks. I hope you
find them useful as well.

The following sections talk about what kind of scripts are available and how to install them

### ImageMagick

- [White-padding image to make it square](./imagemagick/add-white-padding.sh)
- [Convert an image to a favicon](./imagemagick/convert-to-favicon.sh)

### Docker

- [Remove all local containers](./docker/docker-clean-containers.sh)
- [Remove all local containers and images](./docker/docker-clean.sh)

### Druid

- [Druid Cluster Management](./druid)

### Yahoo Fili

- [Use a locally-build Fili-Snapshot](./fili/use-local-fili.sh)

### Webservice

- [Manually deploy a WAR to Jetty Servleet container](./webservice/manually-deploy-webservice.sh)

### How to Use/Install Scripts

#### Mac

At this moment all scripts could be [automatically installed](./install-all-scripts.sh) on Mac:

    cd aergia
    ./install-all-scripts.sh jack
    
where `jack` will be changed to the name of your user that have permission to execute all scripts. In this case, for
example, all scripts will have `jack` as the owner and can be executed normally:

    ls -l add-white-padding.sh 
    -rw-rw-r--. 1 jack admin 0 Feb 26 07:08 add-white-padding.sh 
    
Note that without specifying owner, [install script](./install-all-scripts.sh) may install the scripts to be executable
by `root`

    ls -l add-white-padding.sh 
    -rw-rw-r--. 1 root admin 0 Feb 26 07:08 add-white-padding.sh
    
In this case, user would not be able to run the script. This is why we would ask you to provide the system user name
as the argument to the [install script](./install-all-scripts.sh)

#### Other Systems

Users shall be able to manually put each shell scripts anywhere and execute them in anyway they would like from there.

## License
The use and distribution terms for this software are covered by the Apache License, Version 2.0 (
http://www.apache.org/licenses/LICENSE-2.0.html).
